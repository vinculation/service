"use strict";

const fs = require("fs");
const DbService	= require("moleculer-db")
require('dotenv').config()

if (!global.conf) {
    const conf = require("../config.json")
    global.conf = conf[conf.env]
}

const conn = global.conf["connections"]["utt-vinculation"]


console.log('conn ', conn)

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = function(collection) {

	const cacheCleanEventName = `cache.clean.${collection}`;

	const schema = {
		mixins: [DbService],

		events: {
			/**
			 * Subscribe to the cache clean event. If it's triggered
			 * clean the cache entries for this service.
			 *
			 * @param {Context} ctx
			 */
			async [cacheCleanEventName]() {
				if (this.broker.cacher) {
					await this.broker.cacher.clean(`${this.fullName}.*`);
				}
			}
		},

		methods: {
			/**
			 * Send a cache clearing event when an entity changed.
			 *
			 * @param {String} type
			 * @param {any} json
			 * @param {Context} ctx
			 */
			async entityChanged(type, json, ctx) {
				ctx.broadcast(cacheCleanEventName);
			}
		},

		async started() {
			// Check the count of items in the DB. If it's empty,
			// call the `seedDB` method of the service.
			if (this.seedDB) {
				const count = await this.adapter.count();
				if (count == 0) {
					this.logger.info(`The '${collection}' collection is empty. Seeding the collection...`);
					await this.seedDB();
					this.logger.info("Seeding is done. Number of records:", await this.adapter.count());
				}
			}
		}
	};

	console.log('process.env.MONGO_URI ', process.env.MONGO_URI)

	if (process.env.MONGO_URI) {
		// Mongo adapter
		const MongoAdapter = require("moleculer-db-adapter-mongo");
		let connectonString = "mongodb://";

		if(conn.user && conn.password){
			connectonString = `${conn.user}:${conn.password}@`
		}

		connectonString += `${conn.host}:${conn.port || '27017'}/${conn.database}?ssl=false&authSource=admin`, {useNewUrlParser:true, useUnifiedTopology:true}

		schema.adapter = new MongoAdapter(connectonString);
		schema.collection = collection;
	} else if (process.env.NODE_ENV === 'test') {
		// NeDB memory adapter for testing
		schema.adapter = new DbService.MemoryAdapter();
	} else {
		// NeDB file DB adapter

		// Create data folder
		if (!fs.existsSync("./data")) {
			fs.mkdirSync("./data");
		}

		schema.adapter = new DbService.MemoryAdapter({ filename: `./data/${collection}.db` });
	}

	return schema;
};
