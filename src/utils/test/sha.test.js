import { sha1, sha256, uuidv4 } from '../sha';

describe('sha1', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => sha1('test')).not.toThrow();
  });

  it('should throw if passed invalid store shape', () => {
    expect(() => sha1()).toThrow();
    expect(() => sha1(1)).toThrow();
    expect(() => sha1({})).toThrow();
  });
});

describe('sha256', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => sha256('test')).not.toThrow();
  });

  it('should throw if passed invalid store shape', () => {
    expect(() => sha256()).toThrow();
    expect(() => sha256(1)).toThrow();
    expect(() => sha256({})).toThrow();
  });
});

describe('uuidv4', () => {
  it('should not throw if passed valid store shape', () => {
    expect(() => uuidv4()).not.toThrow();
  });
});
