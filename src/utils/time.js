import dayjs from 'dayjs';
import localizedFormat from 'dayjs/plugin/localizedFormat';

dayjs.extend(localizedFormat);

export const getDateFormated = () => dayjs().format('L');

export const getGreetingTime = () => {
  let g = null;
  const splitAfternoon = 12;
  const splitEvening = 17;
  const currentHour = parseFloat(dayjs().format('HH'));

  if (currentHour >= splitAfternoon && currentHour <= splitEvening) {
    g = 'Good afternoon';
  } else if (currentHour >= splitEvening) {
    g = 'Good evening';
  } else {
    g = 'Good morning';
  }
  return g;
};
