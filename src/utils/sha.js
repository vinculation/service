import crypto from 'crypto';

export const sha1 = (str) => crypto.createHash('sha1').update(str, 'binary').digest('hex');
export const sha256 = (str) => crypto.createHash('sha256').update(str, 'binary').digest('hex');
export const uuidv4 = () => 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
  const r = Math.random() * 16 | 0; // eslint-disable-line no-bitwise
  const v = c === 'x' ? r : (r & 0x3 | 0x8); // eslint-disable-line no-mixed-operators,no-bitwise

  return v.toString(16);
});
