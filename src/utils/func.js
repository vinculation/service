export const isObjectId = (str) => /^[0-9a-fA-F]{24}$/.test(str);

export const removeDuplicates = (data, key) => (
  [
    ...new Map(
      data.map((x) => [key(x), x]),
    ).values(),
  ]
);

export const getFileNameFromDisposition = (disposition) => {
  let filename = '';
  if (disposition && disposition.indexOf('attachment') !== -1) {
    const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
    const matches = filenameRegex.exec(disposition);

    if (matches !== null && matches[1]) {
      filename = matches[1].replace(/['"]/g, '');
    }
  }

  return filename;
};
