import { createSlice } from '@reduxjs/toolkit';

import API from 'services/api';

import { sha256 } from 'utils/sha';

// * Slice
const slice = createSlice({
  name: 'users',
  initialState: {
    checking: false,
    isLoading: false,
    session: JSON.parse(localStorage.getItem('@@wms-session')),
    permissions: sessionStorage.getItem('@@menu')
      ? JSON.parse(sessionStorage.getItem('@@menu'))
      : [],
  },
  reducers: {
    toggleChecking: (state) => {
      state.checking = !state.checking;
    },
    signIn: (state, action) => {
      const { permissions } = action.payload;

      state.checking = false;
      state.session = action.payload;
      state.permissions = permissions;

      localStorage.setItem('@@wms-session', JSON.stringify(action.payload));
      sessionStorage.setItem('@@menu', JSON.stringify(permissions));
    },
    signOut: (state) => {
      state.session = null;
      state.checking = false;
      localStorage.removeItem('@@wms-session');
      sessionStorage.removeItem('@@menu');
    },
  },
});

// * Actions
const {
  signIn,
  signOut,
  toggleChecking,
} = slice.actions;

export const login = ({ email, password }, callback) => (dispatch) => {
  const params = {
    email,
    password: sha256(password),
    environment: '6036f2e672208214676e15d0',
  };
  return API.post('/sign-in', params)
    .then((resp) => resp.data)
    .then((data) => dispatch(signIn(data)))
    .catch(callback);
};

export const logout = () => (dispatch) => dispatch(signOut());

export const checkingSession = () => (dispatch) => {
  dispatch(toggleChecking());
  const session = JSON.parse(localStorage.getItem('@@wms-session'));

  if (session) {
    dispatch(signIn(session));
  } else {
    dispatch(signOut());
  }
};

export const signWithGoogle = (callback) => (dispatch) => {
  authGoogleAccount()
    .then((response) => {
      const { credential, additionalUserInfo } = response;

      const params = {
        email: additionalUserInfo.profile.email,
        token: credential.accessToken,
        environment: '6036f2e672208214676e15d0',
      };

      API.post('/sign-in-google', params)
        .then((result) => dispatch(signIn(result.data)))
        .catch(callback);
    })
    .catch(callback);
};

export const { initialState } = slice;
export default slice.reducer;
