/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

const ProtectedRoute = ({
  isLogIn,
  component: Component,
  redirect,
  ...rest
}) => {
  const { session } = useSelector((state) => state.users);

  return (
    <Route
      {...rest}
      component={(props) => {
        if (isLogIn && session) return <Redirect to="/" />;
        if (isLogIn && !session) return <Component {...props} />;
        return (
          session
            ? (<Component {...props} />)
            : (<Redirect to={redirect} />)
        );
      }}
    />
  );
};

ProtectedRoute.propTypes = {
  isLogIn: PropTypes.bool,
  component: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.shape({}),
    PropTypes.func,
  ]),
  redirect: PropTypes.string,
};

ProtectedRoute.defaultProps = {
  isLogIn: false,
  component: {},
  redirect: '/sign-in',
};

export default ProtectedRoute;
