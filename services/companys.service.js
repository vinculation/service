"use strict";

const DbMixin = require("../mixins/db.mixin");

module.exports = {
	name: "companys",
	mixins: [DbMixin("companys")],
	settings: {
        fields: [
            "_id",
            "name_company",
            "phone",
            "contact",
            "email",
            "business_activity",
            "agreement",
            "comments",
            "active",
        ],
		entityValidator: {
			name_company: { type: "string", optional: false, min: 5, max: 100},
            phone: { type: "number", optional: true },
            contact: { type: "string", optional: true, min: 5, max: 50 },
            email: { type: "email", optional: true, min: 5, max: 50 },
            business_activity: { type: "string", optional: false, min: 3, max: 15 },
            agreement: { type: "boolean", default: true, optional: false },
            comments: { type: "string", optional: true, min: 5, max: 100 },
            active: { type: "boolean", default: true, optional: false },
		}
	},
	hooks: {
		before: {
			create(ctx) {
				ctx.params.quantity = 0;
			}
		}
	},
	actions: {
        //actions
	},
	methods: {
		async seedDB() {
			await this.adapter.insertMany(
                [
                    {
                        name_company:" ADVANCIO, S.A DE C.V",
                        phone: 664125336,
                        contact: "VICTOR GARCIA",
                        email: "rh@agtruckinginc.com",
                        business_activity: "SERVICIO-TRANSPORTE",
                        agreement: true,
                        comments: "blablabla",
                        active: true
                    }
                ]
            );
		}
	},

	async afterConnected() {
		// await this.adapter.collection.createIndex({ name: 1 });
	}
};
